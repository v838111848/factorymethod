﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFactory
{
    class Program
    {
        #region Product
        public interface IProduct
        {
            void Operation();
        }

        public class ProductA : IProduct
        {
            public void Operation()
            {
                Console.WriteLine("A is chosen");
            }
        }

        public class ProductB : IProduct
        {
            public void Operation()
            {
                Console.WriteLine("B is chosen");
            }
        }

        public class ProductDefault : IProduct
        {
            public void Operation()
            {
                Console.WriteLine("Product not exist");
            }
        }
        #endregion

        #region Factory
        public class ProductFactory
        {
            public IProduct CreateProduct(string productType)
            {
                IProduct product;

                //決定要實例化哪種產品，
                if (productType == "A")
                {
                    return product = new ProductA();
                }
                else if (productType == "B")
                {
                    return product = new ProductB();
                }
                else
                {
                    return product = new ProductDefault();
                }
            }
        }
        #endregion


        static void Main(string[] args)
        {
            ProductFactory factory = new ProductFactory();
            while(true)
            {
                Console.Write("輸入產品名稱：");
                string type = Console.ReadLine();
                IProduct product = factory.CreateProduct(type);
                product.Operation();
                Console.WriteLine("");
            }
        }
    }
}
