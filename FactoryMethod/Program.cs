﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    class Program
    {
        #region Product
        public interface IProduct
        {
            void Operation();
        }
        public class ProductA : IProduct
        {
            public void Operation()
            {
                Console.WriteLine("A is chosen");
            }
        }

        public class ProductB : IProduct
        {
            public void Operation()
            {
                Console.WriteLine("B is chosen");
            }
        }
        #endregion

        #region Factory
        public interface IProductFactory
        {
            IProduct CreateProduct();
        }

        public class AFactory : IProductFactory
        {
            public IProduct CreateProduct()
            {
                //AFactory only produce ProductA
                return new ProductA();
            }
        }
        public class BFactory : IProductFactory
        {
            public IProduct CreateProduct()
            {
                //BFactory only produce ProductB
                return new ProductB();
            }
        }
        #endregion

        static void Main(string[] args)
        {
            IProductFactory  factory = new  AFactory();
            IProduct product = factory.CreateProduct();
            Console.WriteLine("AFactory CreateProduct: " + product.GetType());

            Console.WriteLine("");
            factory = new BFactory();
            product = factory.CreateProduct();
            Console.WriteLine("BFactory CreateProduct: " + product.GetType());
            Console.ReadLine();
        }
    }
}


